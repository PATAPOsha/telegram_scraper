import hashlib

import pytz
import logging
import datetime

from hotqueue import HotQueue
from redis import Redis
from telethon.tl.types import Channel
from .config import MESSAGE_LEN_MINIMUM

try:
    import cPickle as pickle
except ImportError:
    import pickle

logger = logging.getLogger()


class HotQueue(HotQueue):
    def __init__(self, name, serializer=pickle, connection=None, **kwargs):
        self.name = name
        self.serializer = serializer
        if connection:
            self.__redis = connection
        else:
            self.__redis = Redis(**kwargs)

    @property
    def key(self):
        """Return the key name used to store this queue in Redis."""
        return self.name

    def get_all(self):
        msgs = self.__redis.lrange(self.key, 0, -1)
        if msgs and self.serializer:
            return [self.serializer.loads(msg) for msg in msgs]
        else:
            return msgs


def process_doc(sender, message, chat, edited=False):
    if not message.raw_text or len(message.raw_text) <= MESSAGE_LEN_MINIMUM:
        return None

    if isinstance(sender, Channel):
        author = sender.title
    else:
        author = ' '.join(x for x in [sender.first_name, sender.last_name] if x)
        if sender.username:
            author += f' (@{sender.username})'

    if isinstance(chat, Channel) and chat.username:
        url = f'https://t.me/{chat.username}/{message.id}'
    else:
        url = ''

    doc = {
        'tg_id': f'{chat.id}_{message.id}',
        'title': chat.title,
        'text': message.raw_text,
        'url': url,
        'post_id': str(message.id),
        'author': author,
        'author_id': str(sender.id),
        'source': chat.title,
        'source_id': str(chat.id),
    }

    if not edited:
        doc.update({
            'created': message.date.replace(microsecond=0).isoformat(),
            'scanned': datetime.datetime.utcnow().replace(microsecond=0).replace(tzinfo=pytz.UTC).isoformat(),
        })
    else:
        doc.update({
            'created': message.date.replace(microsecond=0).isoformat(),
            'updated':message.edit_date.replace(microsecond=0).replace(tzinfo=pytz.UTC).isoformat(),
            'scanned': datetime.datetime.utcnow().replace(microsecond=0).replace(tzinfo=pytz.UTC).isoformat(),
        },
        )

    content_hash = hashlib.md5(doc['author'].encode('utf-8') + doc['text'].encode('utf-8')).hexdigest()
    doc['content_hash'] = content_hash

    return doc


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

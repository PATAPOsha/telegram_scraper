from telethon import sync
from redis import Redis
from teleredis import RedisSession

from .scraper import TelegramScrapper
from . import config

redis_conn = Redis(host=config.REDIS_CONFIG['host'],
                   port=config.REDIS_CONFIG['port'],
                   db=config.REDIS_CONFIG['db'])


def _create_session(phone_number, connect=True):
    session = RedisSession(phone_number, redis_conn)
    client = TelegramScrapper(session, config.API_ID, config.API_HASH)
    if connect:
        client.connect()
    return client


def request_code(phone_number):
    client = _create_session(phone_number)
    if not client.is_user_authorized():
        try:
            r = client.send_code_request(phone_number, force_sms=True)
        except Exception as e:
            client.logger.error(str(e))
            return False, str(e)

        if r.phone_code_hash:
            client.session.redis_connection.set(client.session.sess_prefix + ':sms_hash', r.phone_code_hash)
            msg = f'Successfully requested SMS authorization for {phone_number}'
            client.logger.info(msg)
            return True, msg
        else:
            msg = f'Failed request SMS authorization for {phone_number} (no phone_code_hash found)'
            client.logger.error(msg)
            return False, msg
    else:
        msg = f'User is already authorized: {phone_number}'
        client.logger.error(msg)
        return False, msg


def authorize_user(phone_number, sms_code):
    client = _create_session(phone_number)

    phone_code_hash = client.session.redis_connection.get(client.session.sess_prefix + ':sms_hash').decode()
    if phone_code_hash:

        try:
            user = client.sign_in(phone_number, sms_code, phone_code_hash=phone_code_hash)
        except Exception as e:
            client.logger.error(str(e))
            return False, str(e)

        if user:
            msg = f'Successfully authorized: {phone_number}'
            client.logger.info(msg)
            status = True
        else:
            msg = f'Failed to authorize user {phone_number} with code {sms_code}'
            client.logger.error(msg)
            status = False

        client.session.redis_connection.delete(client.session.sess_prefix + ':sms_hash')
        return status, msg
    else:
        msg = f'No sms_hash found for {phone_number}'
        client.logger.error()
        return False, msg


def check_authorization(phone_number):
    client = _create_session(phone_number, connect=False)
    return bool(client.session.auth_key.key)


def get_chats_from_cache(phone_number):
    client = _create_session(phone_number, connect=False)
    return client.subscribed_channels


def join_chat(phone_number, chat, **kwargs):
    client = _create_session(phone_number, connect=False)
    client.put_chat_to_join_queue(chat, **kwargs)

# Telegram scraper

Collects messages from Telegram channels, groups. Module provides extra functionality for creating Telegram account, authorizing through SMS, subscribing public/private channels and groups.

Scraper uses MongoDB for saving documents, Redis for storing cache stuff (sessions, chat join queue).

Module also uses Bloom Filter for filtering repeating messages from different channels.

# Installation

Was developed and tested on Python-3.6

1) Clone or download this repo
2) Inside your virtualenv run
`pip install -r requirements.txt`
3) Configure `config.py` (set your mongo, redis and telegram app configuration)
4) Put module into your `site-packages`

# Basic usage

Authorizing existing telegram account:

```python
from telegram_scraper.sync_tasks import request_code, authorize_user

# request SMS auth
status, msg = request_code('+380xxxxxxxx')

# enter code from SMS
status, msg = authorize_user('+380xxxxxxxx', 'CODE_FROM_SMS')

```

Start telegram scraper


```python
import asyncio
from telegram_scraper import async_tasks

loop = asyncio.get_event_loop()

loop.create_task(async_tasks.run_client('+380xxxxxxxx'))

try:
    loop.run_forever()
finally:
    loop.close()
```

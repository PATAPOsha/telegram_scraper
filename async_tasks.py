import logging
from redis import Redis
from teleredis import RedisSession

from .events import on_message, on_message_edited
from .scraper import TelegramScrapper
from . import config

logger = logging.getLogger(__name__)

redis_conn = Redis(host=config.REDIS_CONFIG['host'],
                   port=config.REDIS_CONFIG['port'],
                   db=config.REDIS_CONFIG['db'])


async def _create_session(phone_number, connect=True):
    session = RedisSession(phone_number, redis_conn)
    client = TelegramScrapper(session, config.API_ID, config.API_HASH)
    if connect:
        await client.connect()
    return client


async def run_client(phone_number):
    logger.info(f'Starting client with phone number: {phone_number}')
    client = await _create_session(phone_number)

    if not await client.is_user_authorized():
        msg = f'Client is not authorized. Authorize through admin site: {phone_number}'
        client.logger.error(msg)
        client.set_mongo_disabled(log_msg=msg)
        return

    client.add_event_handler(on_message)
    client.add_event_handler(on_message_edited)

    await client.get_new_messages()
    await client.start_chat_joining()
    await client.start_docs_saving()
    await client.start_bans_check()

    try:
        await client.run_until_disconnected()
    finally:
        client.disconnect()


async def request_code(phone_number):
    client = await _create_session(phone_number)
    if not await client.is_user_authorized():
        r = await client.send_code_request(phone_number, force_sms=True)
        if r.phone_code_hash:
            client.session.redis_connection.set(client.session.sess_prefix + ':sms_hash', r.phone_code_hash)
            client.logger.info(f'Successfully requested SMS authorization for {phone_number}')
    else:
        client.logger.error(f'User is already authorized: {phone_number}')


async def authorize_user(phone_number, sms_code):
    client = await _create_session(phone_number)

    phone_code_hash = client.session.redis_connection.get(client.session.sess_prefix + ':sms_hash').decode()
    if phone_code_hash:

        user = await client.sign_in(phone_number, sms_code, phone_code_hash=phone_code_hash)

        if user:
            client.logger.info(f'Successfully authorized: {phone_number}')
        else:
            client.logger.error(f'Failed to authorize user {phone_number} with code {sms_code}')

        client.session.redis_connection.delete(client.session.sess_prefix + ':sms_hash')
    else:
        client.logger.error(f'No sms_hash found for {phone_number}')


async def join_chat(phone_number, chat, **kwargs):
    client = await _create_session(phone_number, connect=False)
    client.put_chat_to_join_queue(chat, **kwargs)


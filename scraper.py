import asyncio
import logging
import random

from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError
from telethon import TelegramClient
from telethon.errors import FloodWaitError, ChannelPrivateError, InviteHashInvalidError, InviteHashExpiredError, \
    UserAlreadyParticipantError, ChannelsTooMuchError, ChatAdminRequiredError, UnauthorizedError
from telethon.tl.functions.channels import JoinChannelRequest
from telethon.tl.functions.messages import CheckChatInviteRequest, ImportChatInviteRequest


from . import config
from .helpers import HotQueue, process_doc, chunks

from socionet.pyrorebloom import RotatingBloomFilter


class TelegramScrapper(TelegramClient):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # prefix for storing subscribed channel ids and last channel message
        # channel_id => last_msg_id
        self.channels_key = self.session.sess_prefix + ':' + config.REDIS_CHANNEL_PREFIX

        self.chat_join_queue = HotQueue(self.session.sess_prefix + ':join_chats_queue',
                                        connection=self.session.redis_connection)

        self.save_docs_queue = asyncio.Queue()

        self.logger = logging.getLogger(__name__ + ' - ' + self.session.session_name)

        client = MongoClient(host=config.MONGO_CONFIG['host'],
                             port=config.MONGO_CONFIG['port'],
                             connect=config.MONGO_CONFIG.get('connect', False))
        self.db = client[config.MONGO_CONFIG['db']]

        self.bloom_filter = RotatingBloomFilter(config.BLOOM_CONFIG['capacity'],
                                                config.BLOOM_CONFIG['error_rate'],
                                                config.BLOOM_CONFIG['count'],
                                                config.BLOOM_CONFIG['redis_key'],
                                                host=config.BLOOM_CONFIG['host'],
                                                port=config.BLOOM_CONFIG['port'],
                                                password=config.BLOOM_CONFIG.get('password', ''),
                                                db=config.BLOOM_CONFIG['db'])

    # Overrode method of `telethon.client.users.UserMethods.__call__(self, request, ordered=False)`
    # In order to handle `UnauthorizedError` for every request
    async def __call__(self, *args, **kwargs):
        try:
            return await super().__call__(*args, **kwargs)
        except UnauthorizedError as e:
            self.logger.error(str(e))

            if any(msg in e.message for msg in ['USER_DEACTIVATED_BAN', 'PHONE_NUMBER_BANNED']):
                self.set_mongo_banned()
            else:
                self.set_mongo_disabled()

            self.disconnect()

    @property
    def subscribed_channels(self):
        """
        :return: list of channel ids user is subscribed
        """
        return [int(x) for x in self.session.redis_connection.hgetall(self.channels_key)]

    def update_last_msg(self, channel_id, msg_id):
        r = self.session.redis_connection.hset(self.channels_key, channel_id, msg_id)
        return r

    def channel_last_msg(self, channel_id):
        msg_id = self.session.redis_connection.hget(self.channels_key, channel_id)
        if msg_id:
            return int(msg_id)
        else:
            return None

    def del_channel_from_cache(self, channel_id):
        return self.session.redis_connection.hdel(self.channels_key, channel_id)

    async def _process_save_docs(self, sleep_time=30):
        self.logger.info('Started _process_save_docs task')
        while True:
            docs = []
            for _ in range(self.save_docs_queue.qsize()):
                doc = await self.save_docs_queue.get()
                if doc:
                    docs.append(doc)
            if docs:
                for_check = [x['content_hash'] for x in docs]
                present_hashes = self.bloom_filter.intersection(for_check)

                filtered = []
                new_docs = []
                for doc in docs:
                    if doc['content_hash'].encode() not in present_hashes:
                        doc.pop('content_hash')
                        new_docs.append(doc)
                    else:
                        filtered.append(doc['s_id'])

                self.logger.info(f'Filtered {len(present_hashes)} docs. ({filtered})')

                if new_docs:
                    self.bloom_filter.update(for_check)

                    for chunk in chunks(new_docs, 500):
                        # save_documents.apply_async(kwargs={'data': chunk})
                        self.logger.info(f'Saved {len(chunk)} new docs. ({[x["s_id"] for x in chunk]})')
                        self.db.test_tg.docs.insert_many(chunk)

            await asyncio.sleep(sleep_time)

    async def _process_chat_join_queue(self):
        self.logger.info('Started _process_chat_join_queue task')
        while self.is_connected():
            sleep_time = config.CHANNEL_JOIN_SLEEP + random.uniform(-10, 10)
            chat_to_join = self.chat_join_queue.get()
            if chat_to_join:
                try:
                    if chat_to_join['private']:
                        r = await self._join_private_chat(chat_to_join['chat'])
                    else:
                        r = await self._join_public_chat(chat_to_join['chat'])
                except FloodWaitError as e:
                    sleep_time = int(str(e).split('A wait of ')[1].split(' ', 1)[0])
                    r = True
                    self.logger.debug(f'Sleeping for {sleep_time} seconds (flood wait on JoinChannelRequest)')
                    self.chat_join_queue.put(chat_to_join)
                if r:
                    await asyncio.sleep(sleep_time)
            else:
                await asyncio.sleep(0.1)

    async def _process_bans_check(self):
        self.logger.info('Started _process_bans_check task')
        while self.is_connected():
            await asyncio.sleep(config.BAN_CHECK_SLEEP)

            await self._check_bans()

    async def _update_participants_info(self, chat):
        try:
            count = await self.get_participants(chat, limit=0)
        except ChatAdminRequiredError:
            self.logger.error('ChatAdminRequiredError')
            self.logger.error(chat)
            return
        if count.total:
            self.db.source.update_one({'sid': f'g{chat.id}'},
                                      {'$set': {'coverage_volume': count.total}})

    async def start_chat_joining(self):
        if self.is_connected():
            self._loop.create_task(self._process_chat_join_queue())
        else:
            raise Exception('Not connected')

    async def start_docs_saving(self):
        if self.is_connected():
            self._loop.create_task(self._process_save_docs())
        else:
            raise Exception('Not connected')

    async def start_bans_check(self):
        if self.is_connected():
            self._loop.create_task(self._process_bans_check())
        else:
            raise Exception('Not connected')

    def _add_subscribed_channel(self, channel_id):
        if isinstance(channel_id, int):
            return self.session.redis_connection.hset(self.channels_key, channel_id, 0)

        elif isinstance(channel_id, list):
            pipe = self.session.redis_connection.pipeline()
            for x in channel_id:
                pipe.hset(self.channels_key, x, 0)
            return pipe.execute()

    async def get_channels(self):
        dialogs = await self.get_dialogs()
        self.logger.info(f'Subscribed for {len(dialogs)} dialogs.')
        channels = []
        for dialog in dialogs:
            if dialog.is_channel or dialog.is_group:
                channel = dialog.entity
                await self._update_participants_info(channel)
                channels.append(channel)
        self.logger.info(f'Subscribed for {len(channels)} channels.')
        return channels

    async def get_new_messages(self):
        self.logger.info('Checking for new messages since prev stop')
        channels = await self.get_channels()
        for channel in channels:
            last_msg = self.channel_last_msg(channel.id)
            if last_msg:
                try:
                    messages = await self.get_messages(channel, min_id=last_msg, max_id=0)
                except ChannelPrivateError:
                    self._banned(channel.id)
                    continue

                self.logger.info(f'{len(messages)} new messages from chat_id: {channel.id}.')
                if messages:
                    for message in messages:
                        if message.from_id:
                            sender = await message.get_sender()
                        else:
                            sender = channel

                        doc = process_doc(sender, message=message, chat=channel)
                        if doc:
                            await self.save_docs_queue.put(doc)

                    self.update_last_msg(channel.id, messages[-1].id)
                    await self.send_read_acknowledge(channel)

        channel_ids = [x.id for x in channels]
        self._add_subscribed_channel(channel_ids)
        await self._check_bans(channel_ids)

    async def _check_bans(self, channel_ids=None):
        if channel_ids is None:
            channel_ids = [x.id for x in await self.get_channels()]

        for cached_channel in self.subscribed_channels:
            if cached_channel not in channel_ids:
                self._banned(cached_channel)

    def _banned(self, channel_id):
        self.del_channel_from_cache(channel_id)
        self.db.telegram_channel.update_one({'tg_id': channel_id},
                                            {'$set': {'status': 'ban'},
                                             '$addToSet': {'ban_history': self.session.session_name}})
        self.logger.warning(f'Banned from channel_id: {channel_id}')

    def _ban_all_channels(self):

        for channel_id in self.subscribed_channels:
            self.del_channel_from_cache(channel_id)

        update_result = self.db.telegram_channel.update_many({'profile': self.session.session_name},
                                                             {'$set': {'status': 'ban'},
                                                              '$addToSet': {'ban_history': self.session.session_name}})

        if update_result.matched_count == 0:
            self.logger.error('Failed to update channels statuses in mongo. No channels found.')
            return

        if update_result.modified_count == 0:
            self.logger.error('Failed to update channels statuses in mongo. Profile found but failed to modify.')

        return True

    async def _join_public_chat(self, chat_name):
        try:
            chat = await self.get_entity(chat_name)
        except (ValueError, ChannelPrivateError) as e:
            msg = e.args[0]
            self.logger.error(msg)
            self.db.telegram_channel.update_one({'username': chat_name},
                                                {'$set': {'status': 'error',
                                                          'log': msg}})
            return

        if chat.id in self.subscribed_channels:
            self.logger.warning(f'Already subscribed to chat_id: {chat.id}')
            return

        try:
            update = await self(JoinChannelRequest(chat))

        except ChannelsTooMuchError as e:
            msg = e.args[0]
            self.logger.error(msg)
            self.db.telegram_channel.update_one({'username': chat_name},
                                                {'$set': {'status': 'error',
                                                          'log': msg}})
            return
        except ChannelPrivateError:
            self._banned(chat.id)
            return

        if chat.id in [x.id for x in update.chats]:
            self.logger.info(f'Successfully joined to "{chat.title}" chat_id: {chat.id}')
            self._add_subscribed_channel(chat.id)
            self.db.telegram_channel.update_one({'username': chat_name},
                                                {'$set': {'tg_id': chat.id,
                                                          'status': 'success'}})

            try:
                self.db.source.insert_one({
                    'sid': f'g{chat.id}',
                    'name': chat.title,
                    'url': f'https://t.me/{chat.username}',
                    'media_type': 'g',
                    'period': '24'
                })
            except DuplicateKeyError:
                pass

            return True

        # probably non-reachable code. TODO: check and delete it after test
        else:
            msg = f'Failed to join chat "{chat.title}" chat_id: {chat.id}' \
                  f'Response: {update.to_dict()}'
            self.logger.error(msg)
            self.db.telegram_channel.update_one({'username': chat_name},
                                                {'$set': {'status': 'error',
                                                          'log': msg}})

    async def _join_private_chat(self, chat_hash):
        try:
            await self(CheckChatInviteRequest(chat_hash))
        except (InviteHashInvalidError, InviteHashExpiredError) as e:
            msg = e.args[0]
            self.logger.error(msg)
            self.db.telegram_channel.update_one({'chat_hash': chat_hash},
                                                {'$set': {'status': 'error',
                                                          'log': msg}})
            return

        try:
            update = await self(ImportChatInviteRequest(chat_hash))
        except (UserAlreadyParticipantError, ChannelsTooMuchError) as e:
            msg = e.args[0]
            self.logger.error(msg)
            self.db.telegram_channel.update_one({'chat_hash': chat_hash},
                                                {'$set': {'status': 'error',
                                                          'log': msg}})
            return

        chat = update.chats[0]
        self.db.telegram_channel.update_one({'chat_hash': chat_hash},
                                            {'$set': {'tg_id': chat.id,
                                                      'status': 'success'}})

        try:
            self.db.source.insert_one({
                'sid': f'g{chat.id}',
                'name': chat.title,
                'url': f'https://t.me/{chat.username}',
                'media_type': 'g',
                'period': '24'
            })
        except DuplicateKeyError:
            pass

        return True

    def put_chat_to_join_queue(self, chat, private=False, update_mongo=True):
        """
        Supposed to be used while client is not connected to telegram.
        :param private: (`bool`):   True - chat_hash is expected, calling _join_private_chat()
                                    False - chat @username is expected, calling _join_public_chat()
        :param chat: (`str`) chat username or chat_hash
        """
        if chat not in [x['chat'] for x in self.chat_join_queue.get_all()]:
            # set status of chat to "pending" in mongo
            if update_mongo:
                if private:
                    selector = {'chat_hash': chat}
                else:
                    selector = {'username': chat}
                update = self.db.telegram_channel.update_one(selector,
                                                             {'$set': {'status': 'pending',
                                                                       'profile': self.session.session_name}})
                if not update.raw_result.get('updatedExisting'):
                    raise Exception('No chat found in mongo.')

            self.chat_join_queue.put({'chat': chat, 'private': private})

        else:
            self.logger.error('Channel is already in queue')

    def set_mongo_disabled(self, log_msg=''):
        disabled_tag = self.db.profile_tag.find_one({'name': 'disabled'})

        update_result = self.db.profile.update_one({'profile_type': 'tg', 'phone': self.session.session_name},
                                                   {'$addToSet': {'tags': disabled_tag['_id']},
                                                    '$set': {'message': log_msg}})
        if update_result.matched_count == 0:
            self.logger.error('Failed to set "disabled_tag" in mongo. No profile found.')
            return

        if update_result.modified_count == 0:
            self.logger.error('Failed to set "disabled_tag" in mongo. Profile found but failed to modify.')

        return True

    def set_mongo_banned(self):
        banned_tag = self.db.profile_tag.find_one({'name': 'blocked'})

        update_result = self.db.profile.update_one({'profile_type': 'tg', 'phone': self.session.session_name},
                                                   {'$addToSet': {'tags': banned_tag['_id']}})

        if update_result.matched_count == 0:
            self.logger.error('Failed to set "banned_tag" in mongo. No profile found.')
            return

        if update_result.modified_count == 0:
            self.logger.error('Failed to set "banned_tag" in mongo. Profile found but failed to modify.')

        self._ban_all_channels()

        return True

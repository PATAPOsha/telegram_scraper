REDIS_CONFIG = {
    'host': 'localhost',
    'port': 6379,
    'db': 11,
}

BLOOM_CONFIG = {
    'host': 'localhost',
    'port': 6379,
    'db': 11,
    'redis_key': 'TG_BLOOM',
    'capacity': 100000,
    'error_rate': 0.00000001,
    'count': 2
}

MONGO_CONFIG = {
    'db': 'tg_db',
    'host': 'localhost',
    'port': 27017,
    'connect': False,
}


API_ID = 'YOUR_APP_ID'
API_HASH = 'YOUR_APP_HASH'

REDIS_CHANNEL_PREFIX = 'channels'

CHANNEL_JOIN_SLEEP = 600
BAN_CHECK_SLEEP = 3600

MESSAGE_LEN_MINIMUM = 5

CHANNELS_PER_PROFILE = 500
